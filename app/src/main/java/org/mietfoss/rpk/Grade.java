package org.mietfoss.rpk;


/**
 * Structure-alike class for storing grades.
 * Contains all necessary fields for storing student's info.
 * If grade is -1, It is considered unset.
 */
public class Grade {
    public int week;
    public String title;
    public int grade; // Actual grade
    public int min; // minimal possible (i.e. Satisfactory)
    public int max; // maximal possible

    public Grade(int week, String title, int grade, int min, int max) {
        this.week = week;
        this.title = title;
        this.grade = grade;
        this.min = min;
        this.max = max;
    }

    public Grade(Grade src) {
        week = src.week;
        title = src.title;
        grade = src.grade;
        min = src.min;
        max = src.max;
    }
}
