package org.mietfoss.rpk;


import android.content.Context;

import java.util.ArrayList;


public class SubjectProvider {

    private ArrayList<Subject> subjects;

    public SubjectProvider() {

    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    /**
     * @TODO
     */
    public void loadCachedSubjects() {
        subjects = new ArrayList<Subject>();

        subjects.add(new Subject(354688, "Дискретная математика"));
        subjects.add(new Subject(40948, "Объектно-ориентированное программирование"));
    }

    public Subject getSubject(int index) {
        return subjects.get(index);
    }

    public ArrayList<String> getSubjectNames() {
        ArrayList<String> result = new ArrayList<String>();
        for (Subject subj : subjects) {
            result.add(subj.getName());
        }
        return result;
    }

}
