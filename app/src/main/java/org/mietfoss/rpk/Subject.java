

package org.mietfoss.rpk;

import java.util.ArrayList;

public class Subject {
    private ArrayList<Grade> grades;

    private int id;

    private String name;

    public Subject(int id, String name) {
        this.id = id;
        this.name = name;
        this.grades = new ArrayList<Grade>();
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public void addGrade(Grade grade) {
        grades.add(grade);
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    /**
     * Walks through all current (set) grades and calculates
     * the total grade
     *
     * @return int
     */
    public int getGradeSum() {
        int sum = 0;
        for (Grade grade : grades) {
            if (grade.grade != -1) {
                sum += grade.grade;
            }
        }
        return sum;
    }

    /**
     * Counts all current grades
     *
     * @return int
     */

    public int getSetGradesCount() {
        int count = 0;
        for (Grade grade : grades) {
            if (grade.grade != -1) {
                count ++;
            }
        }
        return count;
    }

    /**
     * Calculates maximal possible grade
     * at the moment
     *
     * @return int
     */
    public int getMaxGrade() {
        int sum = 0;
        for (Grade grade : grades) {
            if (grade.grade != -1) {
                sum += grade.max;
            }
        }
        return sum;
    }

    /**
     * Recalculates the current grade to the 5-grade system
     * Returns -1 if no grades are set.
     *
     * @return int
     */
    public int calcGrade() {
        int maxGrade = getMaxGrade();
        if (maxGrade == 0) {
            return -1;
        }
        int ratio = getGradeSum() / maxGrade;
        if (ratio > 0.85) {
            return 5;
        }
        else if (ratio >= 0.7) {
            return 4;
        }
        else if (ratio >= 0.5) {
            return 3;
        }
        return 2;
    }

}
